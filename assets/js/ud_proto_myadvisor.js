$(document).ready(function(){
	/* Start Bio Lookup Search functionality*/
	$('#fileUpload').on('click', function() {
		$('#fileUploadHidden').trigger('click');
	});
	$('#fileUploadHidden').change(function() {
		var $thisVal = $(this).val();	
		$('#browserRepo').val($thisVal);
		$('#selectedFile').html($thisVal);
	});
	$('#uploadSave').on('click', function(){
		$('#changeImage').attr('src','assets/images/julie_photo1.png');								  
	});
	$( "#editPhotoForm").find('.btnBar button').on('click', function(){
		$( "#editPhotoForm" ).dialog( "close" );
	})
	var $fbS = $('#blFilterBy'),
		$si = $('#blSearchInp'),
		$blTbl = $('#blDispDataTbl'),
		$srcBtn = $('#isBioLookupForm');
	$fbS.on('change',function(e) {
		var $fbV = $(this).val();
		var datainst =  $('option:selected', $(e.currentTarget)).attr('data-instruction'); 		   
				datainst = (typeof datainst == 'undefined') ? '' : datainst;
				$si.val(datainst); 
	}); 
	$si.on('focus',function(){
		var $cV = $(this).val();
		if($cV == '- Enter Name -' || $cV == '- Enter Title -' || $cV == '- Enter Location -'){ 
			this.value =''; 
		}
	});
	$srcBtn.on('submit',function(e){
			e.preventDefault();
			var $siV = $si.val(),
				$sdV = $('#blFilterBy option:selected').attr('data-instruction');
			if($siV == '' || $siV == '- Enter Name -' || $siV == '- Enter Title -' || $siV == '- Enter Location -' || $sdV == ''){
				$blTbl.addClass('closed');
				$si.focus();
			}else{
				$blTbl.removeClass('closed');
			}
		}); 
	/* End Bio Lookup Search functionality*/
	
	/*Start Add and Remove list functionality*/	
  $('.items li').on('click', function() {
        if($(this).attr('class')=='selected shd1') {
			$(this).removeClass('selected shd1');
		}
		else {
			$(this).addClass('selected shd1');
		}
  });
	
	$('.insertItems').on('click', function() {
		var element_id = $(this).attr('id'),
			primarydiv = element_id.replace('add_', '')+1,
			secondarydiv = element_id.replace('add_', '')+2;
	    $("#"+primarydiv+" ul li input:checkbox[name='contactList_checkbox']:checked").each(function() {
			$(this).closest('ul li').removeClass('selected shd1').appendTo('#'+secondarydiv+' ul');
			$(this).closest('ul li').find('input[type=checkbox]').removeAttr('checked');
		});
	});
	
	$('.deleteItems').on('click', function() {
		var element_id = $(this).attr('id'),
			primarydiv = element_id.replace('remove_', '')+2,
			secondarydiv = element_id.replace('remove_', '')+1;
		$("#"+primarydiv+" ul li input:checkbox[name='contactList_checkbox']:checked").each(function() {
			$(this).closest('ul li').removeClass('selected shd1').appendTo('#'+secondarydiv+' ul');
			$(this).closest('ul li').find('input[type=checkbox]').removeAttr('checked');
		});
	});
	
	/*End Add and Remove list functionality*/	
	
	/*Start Create ne Bio Team List*/
	$('a[href=#teamBio-popup]').on('click', function(){
		$('#disEnbBioSave').addClass('btnOff');		
		$('#nameNewReportBio').val('');		
	});
	$('a[href=#teamcontactList-popup]').on('click', function(){
		$('#disEnbContactSave').addClass('btnOff');		
		$('#nameNewReportContact').val('');		
	});
	
	$('#nameNewReportBio, #nameNewReportContact').on('keypress keyup',function(){
		var $ulChkB = $('#teamBio2 ul');
		var $ulChkC = $('#teamcontactList2 ul');
		var $inLen = $(this).val();
		if($ulChkB.html() == '' && $inLen == '' || $ulChkC.html() == '' && $inLen == ''){
			$('#disEnbBioSave, #disEnbContactSave').addClass('btnOff');	
		}else{
			$('#disEnbBioSave, #disEnbContactSave').removeClass('btnOff');	
		}
	});
	$('#disEnbBioSave').on('click', function(){
		var $fVb = $('#nameNewReportBio').val(),
			$aRow = '<tr><td><a href="#">'+$fVb+'</a><div class="flr"><span class="mts mrlg icclearLink icon"></span><span class="mts mrlg iceditLink icon"></span>'+
					'<span class="mts mrlg icprintLink icon"></span></div></td><td><a href="#">Individual Listing</a></td><td></td></tr>';
		$('#tbl_reports tr:last').after($aRow);
		$('a[href=#closePopup]').trigger('click')
		$('#teamBio2 ul').html('');
	});
	$('#disEnbContactSave').on('click', function(){
		var $fVc = $('#nameNewReportContact').val(),
			$aRow = '<tr><td><a href="#">'+$fVc+'</a><div class="flr"><span class="mts mrlg icclearLink icon"></span><span class="mts mrlg iceditLink icon"></span>'+
					'<span class="mts mrlg icprintLink icon"></span></div></td><td><a href="#">Individual Listing</a></td><td></td></tr>';
		$('#tbl_reports tr:last').after($aRow);
		$('#addLastReport').html($fVc);
		$('a[href=#closePopup]').trigger('click');
		$('#teamcontactList2 ul').html('');
		$('#teamBio2 ul').html('');
	});
	/*End Create ne Bio Team List*/
	
	$('#ebtn').on('click', function() {
		$('.dclass, .eeclass, .epclass').addClass('closed');
		$('.eclass, .esclass').removeClass('closed');
		$('#photoDisp').addClass('closed');
		$('#photoEdit').removeClass('closed');
		$('.addMoreCol').removeClass('closed');
	});
	
	$('#cncl').on('click', function() {
		$('.dclass, .eeclass, .dsclass').removeClass('closed');
		$('.eclass, .esclass, .successMsg, .sclass').addClass('closed');
		$("#offi, #MailingAdd-field, #Suite-field").val('');
	});
	
	$('#sbtn').on('click', function() {
		$(' .sclass, .epclass, .eaclass, .successMsg').removeClass('closed');
		$('.esclass, .dsclass').addClass('closed');
	});
	$('#offi').on('change', function() {
		var chV = $(this).val();
		$("#city").val(chV);
		$("#MailingAdd-field").val('155 Village Blvd');
		$("#Suite-field").val('Suite A');
		$("#state-field").val('NJ');
		$('.sclass').removeClass('closed');
		$('.dsclass').addClass('closed');
	});

		
	$('#fileUpload').on('click', function() {
		$('#fileUploadHidden').trigger('click');
	});
	$('#fileUploadHidden').change(function() {
			var $thisVal = $(this).val();	
			$('#browserRepo').val($thisVal);
	});
	
	$('#additems').on('click', function() {
	    if(!$('#additems').hasClass('btnOff')) {
			$('#reportlist1 li.selected').appendTo('#reportlist2');
			$('#reportlist1 li').length ==0 ? $('#additems').addClass('btnOff'):'';
			$('#reportlist2 li').length >0 ? $('#removeitems').removeClass('btnOff'):'';
		}
	});
	
	$('#removeitems').on('click', function() {
		if(!$('#removeitems').hasClass('btnOff')) {
			$('#reportlist2 li.selected').appendTo('#reportlist1');
			$('#reportlist2 li').length ==0 ? $('#removeitems').addClass('btnOff'):'';
			$('#reportlist1 li').length >0 ? $('#additems').removeClass('btnOff'):'';
		}	
	});
	
	$('#srcprofile').on('click',function() {
		$('#profilestatus').removeClass('closed');
	 }); 
	 var cnt = 4;
	 $('#terri').on('change',function(){
	 cnt++;
	 var $thisV = $(this).val();
		var $insInp = '<div class="lblFieldPair nth mtm"><div class="input">'+
					  '<input id="addlterri'+cnt+'-field" type="text" aria-required="true" name="addlterri'+cnt+'-field" value="'+$thisV+'">'+
					  '</div></div>';
		$(this).closest('div.lblFieldPair').before($insInp);
	 });
});